package com.rezgateway.automation.bonotel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.Difference;

import com.rezgateway.automation.JavaHttpHandler;
import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.builder.request.CancellationRequestBuilder;
import com.rezgateway.automation.builder.request.ReservationRequestBuilder;
import com.rezgateway.automation.input.ExcelReader;
import com.rezgateway.automation.pojo.AvailabilityRequest;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.CancellationRequest;
import com.rezgateway.automation.pojo.CancellationResponse;
import com.rezgateway.automation.pojo.Hotel;
import com.rezgateway.automation.pojo.HttpResponse;
import com.rezgateway.automation.pojo.RateplansInfo;
import com.rezgateway.automation.pojo.ReservationRequest;
import com.rezgateway.automation.pojo.ReservationResponse;
import com.rezgateway.automation.pojo.Room;
import com.rezgateway.automation.reader.response.AvailabilityResponseReader;
import com.rezgateway.automation.reader.response.ReservationResponseReader;
import com.rezgateway.automation.reports.ExtentTestNGReportBuilderExt;
import com.rezgateway.automation.xmlout.utill.DataLoader;

public class ComparisonTest extends ExtentTestNGReportBuilderExt {
	AvailabilityResponse AvailResponse = new AvailabilityResponse();
	ReservationRequest ResRequest = new ReservationRequest();
	ReservationResponse ResResponse = new ReservationResponse();
	CancellationRequest CancelRequest = new CancellationRequest();
	CancellationResponse CancelResponse = new CancellationResponse();
	LocalDateTime date = LocalDateTime.now();
	DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
	FileWriter fw = null;
	
	private static String EX_URL;
	private static String ACT_URL;
	private static String RES_EX_URL;
	private static String RES_ACT_URL;
	private static String CAN_EX_URL;
	private static String CAN_ACT_URL;
	private static boolean ALLOW_RES;
	private static TreeMap<String,String[]> mapResData = new TreeMap<String, String[]>();

	@Parameters({ "ex_url", "act_url", "resEx_url", "resAct_url", "canEx_url", "canAct_url", "allow_res" })
	@BeforeClass()
	public void setUp(String ex_url, String act_url, String resEx_url, String resAct_url, String canEx_url, String canAct_url, boolean allow_res) {

		EX_URL = ex_url;
		ACT_URL = act_url;
		RES_EX_URL  = resEx_url;
		RES_ACT_URL = resAct_url;
		CAN_EX_URL  = canEx_url;
		CAN_ACT_URL = canAct_url;
		ALLOW_RES = allow_res;
		System.out.println("EX_URL --> " + EX_URL);
		System.out.println("ACT_URL --> " + ACT_URL);
		System.out.println("RES_EX_URL --> " + RES_EX_URL);
		System.out.println("RES_ACT_URL --> " + RES_ACT_URL);
		System.out.println("CAN_EX_URL --> " + CAN_EX_URL);
		System.out.println("CAN_ACT_URL --> " + CAN_ACT_URL);
		System.out.println("ALLOW_RES --> " + ALLOW_RES);
	}

	@Test(dataProvider = "AvailabilityData", priority = 0)
	@Parameters
	public void compareAvailabilityXml(AvailabilityRequest request) throws IOException {
		String Scenario     = request.getScenarioID();
		String HotelCode    = Arrays.toString(request.getCode());
		String SearchString = request.getCheckin()+"|"+request.getCheckout()+" |"+request.getNoOfRooms()+" R " ;
		String testname = "<b> Availability xml comparison </b><pre> Scenario \t: " + Scenario + " <br> Search By \t: " + request.getSearchType() + "<br> Hotel code \t: " + HotelCode + 
				"<br> Criteria \t: " + SearchString + "<br> Username \t: " + request.getUserName() + "<br> Password \t: " + request.getPassword() + "</pre>";
		
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", testname);
		
		JavaHttpHandler handler = new JavaHttpHandler();		
		HttpResponse expected_response = handler.sendPOST(EX_URL, "xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/Ex_AvailRequest_senarioID_" + Scenario + ".xml", request));
		HttpResponse actual_response = handler.sendPOST(ACT_URL, "xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/Act_AvailRequest_senarioID_" + Scenario + ".xml", request));

		String exResPath = "Resources/Ex_AvailResponse_senarioID_" + Scenario + ".xml";
		String actResPath = "Resources/Act_AvailResponse_senarioID_" + Scenario + ".xml";
		File exResXml = new File(exResPath);
		File actResXml = new File(actResPath);
		exResXml.createNewFile();		
		fw = new FileWriter(exResPath);
		fw.write(expected_response.getRESPONSE());
		fw.close();
		
		actResXml.createNewFile();
		fw = new FileWriter(actResPath);
		fw.write(actual_response.getRESPONSE());
		fw.close();
		
		if(expected_response.getRESPONSE_CODE() == 200 && actual_response.getRESPONSE_CODE() == 200){
			Diff myDiff = DiffBuilder.compare(expected_response.getRESPONSE()).withTest(actual_response.getRESPONSE())
					.build();

			if (myDiff.hasDifferences()) {
				Iterator<Difference> iter = myDiff.getDifferences().iterator();
				int size = 0;
				String mismatches = "";
				while (iter.hasNext()) {
					String difference = iter.next().toString();
					mismatches += size + "--> " + difference + "\n" ;
					size++;
				}

				result.setAttribute("Expected", "Xmls should be identical...");
				result.setAttribute("Actual", "No of mismatches in availability - " + size);
				Assert.fail("Available mismatches in availability  : \n" + mismatches);
				
			} else {			
				result.setAttribute("Expected", "Availability xmls should be identical...");
				result.setAttribute("Actual", "Availability xmls are identical...");
			}	
		}else {
			result.setStatus(3);
			result.setAttribute("Expected", "Receiving an availability response...");
			result.setAttribute("Actual", "Not receiving a response...");
			throw new SkipException("Not receiving a response...");
		}
	}
	
	
	@Test(dataProvider = "AvailabilityData", priority = 1)
	@Parameters
	public void compareReservationXml(AvailabilityRequest request) throws IOException {
		String Scenario     = request.getScenarioID();
		String HotelCode    = Arrays.toString(request.getCode());
		String SearchString = request.getCheckin()+"|"+request.getCheckout()+" |"+request.getNoOfRooms()+" R " ;
		String testname = "<b> Reservation xml comparison </b><pre> Scenario \t: " + Scenario + " <br> Search By \t: " + request.getSearchType() + "<br> Hotel code \t: " + HotelCode + 
				"<br> Criteria \t: " + SearchString + "<br> Username \t: " + request.getUserName() + "<br> Password \t: " + request.getPassword() + "</pre>";
		
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", testname);
				
		if (ALLOW_RES == true) {
			AvailabilityRequestBuilder builder = new AvailabilityRequestBuilder();
			String request_str = builder.buildRequest(request);
			
			JavaHttpHandler handler = new JavaHttpHandler();
			HttpResponse expected_response = handler.sendPOST(EX_URL, "xml=" + request_str);
			HttpResponse actual_response = handler.sendPOST(ACT_URL, "xml=" + request_str);	
			
			if(expected_response.getRESPONSE_CODE() == 200 && actual_response.getRESPONSE_CODE() == 200){
				Diff myDiff = DiffBuilder.compare(expected_response.getRESPONSE()).withTest(actual_response.getRESPONSE())
						.build();

				if (myDiff.hasDifferences()) {
					Iterator<Difference> iter = myDiff.getDifferences().iterator();
					int size = 0;
					String mismatches = "";
					while (iter.hasNext()) {
						String difference = iter.next().toString();
						mismatches += size + "--> " + difference + "\n" ;
						size++;
					}

					result.setStatus(3);
					result.setAttribute("Expected", "Xmls should be identical...");
					result.setAttribute("Actual", "No of mismatches in availability - " + size);
					throw new SkipException("Available mismatches in availability  : \n" + mismatches);										
				} else {			
					AvailResponse = new AvailabilityResponseReader().getResponse(expected_response.getRESPONSE());
					ResRequest.getRezRoomList().clear();
					if(AvailResponse.getHotelCount() > 0){						
						Hotel hotel = AvailResponse.getHotelList().entrySet().iterator().next().getValue();						
						ResRequest.setCode(request.getCode());
						ResRequest.setCheckin(request.getCheckin());
						ResRequest.setCheckout(request.getCheckout());
						ResRequest.setNoOfRooms(request.getNoOfRooms());
						ResRequest.setNoofNights(request.getNoofNights());
						ResRequest.setCurrency(hotel.getRateCurrencyCode());
						ResRequest.setNoOfRooms(Integer.toString(hotel.getRoomInfo().size()));
						
						Iterator<Entry<String, ArrayList<Room>>> iter = hotel.getRoomInfo().entrySet().iterator();
						
						Double totalRate = 0.00;
						Double totalTax = 0.00;
						int i = 0;
						while (iter.hasNext()) {
							Map.Entry<String, ArrayList<Room>> entry = (Map.Entry<String, ArrayList<Room>>) iter.next();
							
							Room room = entry.getValue().get(i);
							room.setAdultsCount(request.getRoomlist().get(i).getAdultsCount());
							room.setChildCount(request.getRoomlist().get(i).getChildCount());
							room.setChildAges(request.getRoomlist().get(i).getChildAges());
							room.setConType(room.getConType());
							Entry<String, RateplansInfo> ratePlanInfo = room.getRatesPlanInfo().entrySet().iterator().next();
							RateplansInfo rateInfo = ratePlanInfo.getValue();
							room.setRatePlanCode(rateInfo.getRatePlanCode());
							Double roomTax = rateInfo.getTaxInfor().get("roomTax").getTaxAmount();
							Double salesTax = rateInfo.getTaxInfor().get("salesTax").getTaxAmount();
							Double otherCharges = rateInfo.getTaxInfor().get("otherCharges").getTaxAmount();
							totalRate += rateInfo.getTotalRate();
							totalTax += (roomTax + salesTax + otherCharges);
							i++;
							ResRequest.addToRezRoomList(room);
							ResRequest.setConfType((room.getConType()));
						}
						ResRequest.setTotal(Double.toString(totalRate));
						ResRequest.setTotalTax(Double.toString(totalTax));
						ResRequest.setReservationDetailsTimeStamp(date.format(formatter));
												
						ReservationRequestBuilder resBuilder = new ReservationRequestBuilder();
						String resRequest_str = resBuilder.buildRequest(ResRequest);
														
						String nameString = resRequest_str.split("</firstName>")[0];
						String firstname = nameString.substring(Math.max(nameString.length() - 6, 0));
						int no = Integer.parseInt(firstname.substring(firstname.length() - 1));
						String newName = "adult" + (no + 1) ;				
						String resRequest_str2 = resRequest_str.replace(firstname, newName);	
						
						System.out.println("Expected res_request --> " + resRequest_str);
						System.out.println("Actual res_request --> " + resRequest_str2);
						
						HttpResponse expected_resResponse = handler.sendPOST(RES_EX_URL, "xml=" + resRequest_str);
						HttpResponse actual_resResponse = handler.sendPOST(RES_ACT_URL, "xml=" + resRequest_str2);
						
						System.out.println("Expected res_response --> " + expected_resResponse.getRESPONSE());
						System.out.println("Actual res_response --> " + actual_resResponse.getRESPONSE());
						
						if(expected_resResponse.getRESPONSE_CODE() == 200 && actual_resResponse.getRESPONSE_CODE() == 200){
							Diff myResDiff = DiffBuilder.compare(expected_resResponse.getRESPONSE()).withTest(actual_resResponse.getRESPONSE())
									.withNodeFilter(node -> !(node.getNodeName().equals("referenceNo") ||
					                          node.getNodeName().equals("roomReferenceNo") || node.getNodeName().equals("roomResNo") || 
					                          node.getNodeName().equals("firstName"))).build();
							/*Diff myResDiff = DiffBuilder.compare(hardcoded).withTest(actual_resResponse.getRESPONSE())
									.build();*/
							
							if (myResDiff.hasDifferences()) {
								Iterator<Difference> iterRes = myResDiff.getDifferences().iterator();
								int size = 0;
								String mismatches = "";
								while (iterRes.hasNext()) {
									String difference = iterRes.next().toString();
									mismatches += size + "--> " + difference + "\n" ;
									size++;
								}
								
								result.setAttribute("Expected", "Reservation xmls should be identical...");
								result.setAttribute("Actual", "No of mismatches in reservation --> " + size);
								Assert.fail("Available mismatches in reservation  : \n" + mismatches);
								
							} else {
								ResResponse = new ReservationResponseReader().getResponse(expected_resResponse.getRESPONSE());
								String resId_expected = ResResponse.getReferenceno();
								String resId_actual = new ReservationResponseReader().getResponse(actual_resResponse.getRESPONSE()).getReferenceno();
								String resStatus = ResResponse.getRreservationResponseStatus();
								mapResData.put(request.getScenarioID(), new String[] {resStatus, resId_expected, resId_actual});
															
								result.setAttribute("Expected", "Reservation xmls should be identical...");
								result.setAttribute("Actual", "Reservation xmls are identical...");				
							}					
						} else {
							result.setStatus(3);
							result.setAttribute("Expected", "Receiving a reservation response...");
							result.setAttribute("Actual", "Not receiving a response...");
							throw new SkipException("Not receiving a response...");
						}						
					}else {
						result.setStatus(3);
						result.setAttribute("Expected", "Successful availability response should be received...");
						result.setAttribute("Actual", "Error response received in availability...");
						throw new SkipException("Error response received in availability...");	
					}					
				}			
			}else {
				result.setStatus(3);
				result.setAttribute("Expected", "Receiving an availability response...");
				result.setAttribute("Actual", "Not receiving a response...");
				throw new SkipException("Not receiving an availability response...");				
			}			
		}else {
			result.setStatus(3);
			result.setAttribute("Expected", "To make reservations ALLOW_RES should be true...");
			result.setAttribute("Actual", "Making reservations not allowed. ALLOW_RES = " + ALLOW_RES);
			throw new SkipException("Making reservations not allowed...");			
		}		
	}
	
	
	@Test(dataProvider = "AvailabilityData", priority = 2)
	@Parameters
	public void compareCancellationXml(AvailabilityRequest request) throws IOException {
		String Scenario     = request.getScenarioID();
		String HotelCode    = Arrays.toString(request.getCode());
		String SearchString = request.getCheckin()+"|"+request.getCheckout()+" |"+request.getNoOfRooms()+" R " ;
		String testname = "<b> Cancellation xml comparison </b><pre> Scenario \t: " + Scenario + " <br> Search By \t: " + request.getSearchType() + "<br> Hotel code \t: " + HotelCode + 
				"<br> Criteria \t: " + SearchString + "<br> Username \t: " + request.getUserName() + "<br> Password \t: " + request.getPassword() + "</pre>";
		
		ITestResult result = Reporter.getCurrentTestResult();
		result.setAttribute("TestName", testname);
			
		if(mapResData.containsKey(Scenario)){
			String data[] = mapResData.get(Scenario);
			String isSuccessRes = data[0];
			
			if ("Y".equals(isSuccessRes)) {
				String resId_expected = data[1];
				String resId_actual = data[2];
				
				CancelRequest.setCancellationRequestTimestamp(date.format(formatter));
				CancelRequest.setUserName(request.getUserName());
				CancelRequest.setPassword(request.getPassword());
				CancelRequest.setSupplierReferenceNo(resId_expected);
				CancelRequest.setCancellationNotes("Test reservation_" + request.getScenarioID());
				CancelRequest.setCancellationReason("Test reservation");
				
				CancellationRequest CancelRequest2 = new CancellationRequest();
				CancelRequest2.setCancellationRequestTimestamp(date.format(formatter));
				CancelRequest2.setUserName(request.getUserName());
				CancelRequest2.setPassword(request.getPassword());
				CancelRequest2.setSupplierReferenceNo(resId_actual);
				CancelRequest2.setCancellationNotes("Test reservation_" + request.getScenarioID());
				CancelRequest2.setCancellationReason("Test reservation");
				
				CancellationRequestBuilder cnBuilder = new CancellationRequestBuilder();
				String cnRequest_str = cnBuilder.buildRequest(CancelRequest);
				String cnRequest_str2 = cnBuilder.buildRequest(CancelRequest2);
				
				System.out.println("Expected cnRequest_str -- > " + cnRequest_str);
				System.out.println("Actual cnRequest_str2 -- > " + cnRequest_str2);	
				
				JavaHttpHandler handler = new JavaHttpHandler();
				HttpResponse expected_cnResponse = handler.sendPOST(CAN_EX_URL, "xml=" + cnRequest_str);
				HttpResponse actual_cnResponse = handler.sendPOST(CAN_ACT_URL, "xml=" + cnRequest_str2);
				
				System.out.println("Expected cancel res -- > " + expected_cnResponse.getRESPONSE());
				System.out.println("Actual cancel res -- > " + actual_cnResponse.getRESPONSE());	
				
				if(expected_cnResponse.getRESPONSE_CODE() == 200 && actual_cnResponse.getRESPONSE_CODE() == 200){
					Diff myCancelDiff = DiffBuilder.compare(expected_cnResponse.getRESPONSE()).withTest(actual_cnResponse.getRESPONSE())
							.withNodeFilter(node -> !(node.getNodeName().equals("cancellationNo"))).build();
					
					if (myCancelDiff.hasDifferences()) {
						Iterator<Difference> iterRes = myCancelDiff.getDifferences().iterator();
						int size = 0;
						String mismatches = "";
						while (iterRes.hasNext()) {
							String difference = iterRes.next().toString();
							mismatches += size + "--> " + difference + "\n" ;
							size++;
						}

						result.setAttribute("Expected", "Cancellation xmls should be identical...");
						result.setAttribute("Actual", "No of mismatches in Cancellation --> " + size);
						Assert.fail("Available mismatches in Cancellation  : \n" + mismatches);
						
					} else {
						result.setAttribute("Expected", "Cancellation xmls should be identical...");
						result.setAttribute("Actual", "Cancellation xmls are identical...");
					}
					
				} else {
					result.setStatus(3);
					result.setAttribute("Expected", "Receiving a cancellation response...");
					result.setAttribute("Actual", "Not receiving a response...");
					throw new SkipException("Not receiving a response...");
				}				
			} else {
				result.setStatus(3);
				result.setAttribute("Expected", "Receiving a successeful reservation response...");
				result.setAttribute("Actual", "Receiving an error reservation response...");
				throw new SkipException("Receiving an error reservation response...");
			}			
		}else {
			result.setStatus(3);
			result.setAttribute("Expected", "Receiving successeful availability responses...");
			result.setAttribute("Actual", "Not receiving successeful availability response...");
			throw new SkipException("Not receiving successeful availability response......");
		}		
	}
	

	@AfterClass
	public void tearDown() {

	}

	@DataProvider(name="AvailabilityData")
	public Object[][] provideAvailabilityData() throws Exception {
	
		ExcelReader reader = new ExcelReader();
		String[][] data    = reader.getExcelData("Resources/Full Regression Testing Checklist.xls", "scenario");
	
		DataLoader  loader = new DataLoader();
		AvailabilityRequest[][] requests = loader.getAvailabilityObjList(data);
    
		return requests;
	}
	
	private String getTestXML(String testFilePath) {
		try {
			File contFile = new File(testFilePath);
			FileInputStream fis = new FileInputStream(contFile);
			try {				
				if (contFile.exists()) {
					int length = new Long(contFile.length()).intValue();
					byte[] byteSet = new byte[length];
					fis.read(byteSet);
					String response = new String(byteSet);
					return response;
				}
			} finally {
				fis.close();
			}
		} catch (Exception e) {
			System.out.println("Exception --> " + e);
		}
		return null;
	}

}
